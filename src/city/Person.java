package city;

public class Person {
	private String firstName;
	private String LastName;
	private int birhtYear;
	public static boolean toUppercase = false;
	//private boolean IsAdult;
	
	
	public Person(String firstName, String lastName, int birthYear) {
		this.firstName = firstName;
		this.LastName = lastName;
		this.birhtYear = birthYear;
	}
	
	public Person() {
		
	}
	
	public static Person makePerson(String firstName, String lastName, int birthYear) {
		if(firstName == null || lastName == null || birthYear < 1900 || birthYear > 2016) {
			return null;
		}else {
			Person example = new Person();
			example.setBirhtYear(birthYear);
			example.setFirstName(firstName);
			example.setLastName(lastName);
			return example;
		}
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public int getBirhtYear() {
		return birhtYear;
	}

	public void setBirhtYear(int birhtYear) {
		this.birhtYear = birhtYear;
	}
	
	public boolean isAdult(int year) {
		if(year-this.birhtYear <= 17) {
			//this.IsAdult = false;
			return false;
		}else {
			//this.IsAdult = true;
			return true;
		}
	}
	
	public String toString() {
		if(toUppercase) {
			return this.LastName.toUpperCase() + " " + this.firstName.toUpperCase();
		}
		return this.LastName + " " + this.firstName;
	}
	
}
