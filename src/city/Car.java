package city;

import java.util.Calendar;

public class Car {
	private String numberPlate;
	private int numberOfDoors;
	private Person owner;
	
	public Car(String numberPlate, int numberOfDoors, Person owner) {
		this.numberPlate = numberPlate;
		this.numberOfDoors = numberOfDoors;
		this.owner = owner;
	}
	
	public boolean isFamilyCar() {
		if(this.numberOfDoors == 5) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean isValid() {
		if(this.numberPlate.isEmpty() == false
		&& this.numberOfDoors == 3 || this.numberOfDoors == 5
		&& this.owner != null
		&& owner.isAdult(Calendar.getInstance().get(Calendar.YEAR))) {
			return true;
		}else {
			return false;
		}
	}
	
	public String toString() {
		return  this.numberPlate + " " + this.numberOfDoors + " doors, owner: " + this.owner;  
	}

	public String getNumberPlate() {
		return numberPlate;
	}

	public void setNumberPlate(String numberPlate) {
		this.numberPlate = numberPlate;
	}

	public int getNumberOfDoors() {
		return numberOfDoors;
	}

	public void setNumberOfDoors(int numberOfDoors) {
		this.numberOfDoors = numberOfDoors;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}
	
}
