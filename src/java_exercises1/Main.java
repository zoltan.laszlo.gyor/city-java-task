package java_exercises1;

import city.Car;
import city.Person;

public class Main {
	public static void main(String[] args) {
		Person adam = new Person("Adam", "Flisch", 1995);
		Car adamCar = new Car("ABC-123", 5, adam);
		
		System.out.println(adamCar.toString());
		
		
		Person travis = Person.makePerson("Travis", "McGrewery", 2000);
	}
}
