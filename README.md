# City Java Task

Feladatok

1. Készítsünk egy Person osztályt, mely egy személyt fog ábrázolni! A személyekről nyilvántartjuk a vezeték- és keresztnevüket (két String), illetve a születésük évét (int). Legyen egy konstruktora is, mely átveszi az adattagok kezdeti értékeit paraméterül!

        a. Adjunk a Person osztályhoz egy isAdult metódust! Ez egy évszámot vár és logikai igazat ad vissza, ha az adott személy felnőtt korú az adott évben, hamisat, ha kiskorú.

        b. Tegyük lehetővé, hogy egy Person objektumot szöveggé alakíthassunk! Ehhez készítsünk egy toString metódust, mely egymás után fűzi a vezeték- és keresztnevet egy szóközzel elválasztva.

        c. Vegyünk fel egy osztályszintű logikai adattagot, melynek hatására egycsapásra minden személy nevét nagybetűkkel írja ki a toString! 

2. Írjunk egy autókat ábrázoló Car osztályt! Az autókról eltároljuk a rendszámukat (String), hogy hány ajtósak (int) és a tulajdonosukat (Person). Készítsünk hozzá konstruktort is, mely inicializálja a mezőket!

        a. Adjunk a Car osztályhoz egy isFamilyCar metódust, amely eldönti, hogy egy autó családi vagy sem! Családinak mondjuk a járművet, ha 5 ajtaja van.

        b. Készítsünk egy isValid nevű metódust, mely logikai igazat ad vissza, ha az adott Car objektum érvényes! Ez akkor teljesül, ha

    - a rendszám nem egy üres szöveg (ennek eldöntésére használjuk String.isEmpty() metódust),
    - az ajtók száma 3 vagy 5,
    - a tulajdonos nem egy üres referencia, vagyis nem null,
    - a tulajdonos felnőttkorú a mostani évben.
    
            c. Valósítjuk meg a Car objektukot szöveggé alakítását egy toString metódussal! Például egy "ABC-123" rendszámú, ötajtós, Roger Federer tulajdonában álló autó esetén állítsuk elő a következő szöveget:

            "ABC-123 5 doors, owner: Roger Federer"
        
3. Helyezzük el a Person és a Car osztályokat egy city nevű csomagba! Ehhez mozgassuk át a forrásfájlokat egy újonnan létrehozott city könyvtárba és írjuk a fájlok elejére a csomag deklarációt.

4. Írjunk egy main metódust! Helyezzük el ezt egy Main osztályban, melyet tegyük a program csomagba! A main metódus törzsében hozzunk létre egy személy és egy autó objektumot, majd írassuk ki az utóbbit a szabványos kimenetre!

5. Írjuk át a Person konstruktorát egy makePerson nevű statikus metódussá, mely létrehoz egy Person típusú objektumot! Ehhez létre kell hozni egy inicializálatlan Person típusú objektumot és be kell állítani az adattagjait. A makePerson paramétere legyen a személy vezeték- és keresztneve és születési éve!

Ellenőrizzük le, hogy helyesek-e a paraméterek: a név nem lehet üres, a születési év 1900 és 2016 között legyen! Ha nem teljesülnek a feltételek, adjunk vissza egy sehova sem mutató referenciát, azaz null-t!
